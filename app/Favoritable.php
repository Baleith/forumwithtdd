<?php

namespace App;


use Illuminate\Support\Facades\Auth;

trait Favoritable
{

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }

    public function favorite()
    {
        $this->favorites()->create(['user_id' => Auth::id()]);
    }

    public function isFavorited()
    {
        return !!$this->favorites->where('user_id', Auth::id())->count();
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
}