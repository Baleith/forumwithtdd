<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadsTest extends TestCase
{

    use DatabaseMigrations;

    protected $thread;

    public function setUp()
    {
        parent::setUp();

        $this->thread = create('App\Thread');
    }

    /** @test */
    public function a_user_can_browse_all_threads()
    {
        $response = $this->get('/threads');
        $response->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_view_a_thread()
    {
        $response = $this->get("/threads/{$this->thread->channel->slug}/{$this->thread->id}");
        $response->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_read_replies_assoc_with_a_thread()
    {
        $reply = factory('App\Reply')->create(['thread_id' => $this->thread->id]);

        $this->get($this->thread->path())
            ->assertSee($reply->body);
    }
    /** @test */
    public function a_user_can_filter_threads_according_to_a_channel()
    {   
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
        $threadNotInChannel = create('App\Thread');
        
        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
        
    }

    /** @test */
    public function a_user_can_filter_threads_by_username()
    {
        $this->signIn(create('App\User', ['name' => 'JohnDoe']));

        $threadByJohnDoe = create('App\Thread', ['user_id' => Auth::id()]);
        $threadNotByJohnDoe = create('App\Thread');

        $this->get('/threads?by=JohnDoe')
            ->assertSee($threadByJohnDoe->title)
            ->assertDontSee($threadNotByJohnDoe->title);

    }

    /** @test */
    public function a_user_can_filter_post_by_popularity()
    {
        $threadTwoReply = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadTwoReply->id], 2);

        $threadThreeReply = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadThreeReply->id], 3);


        $response = $this->get('threads?popular=1');

        $threadsFromResponse = $response->baseResponse->original->getData()['threads'];

        $this->assertEquals([3,2,0], $threadsFromResponse->pluck('replies_count')->toArray());
    }
}

