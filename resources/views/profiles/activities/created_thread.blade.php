@component('profiles.activities.activity')
    @slot('header')
        <a href="#">{{ $profileUser->name }}</a> published <a href="{{ $activity->subject->path() }}">{{ $activity->subject->title }}</a>
            {{ $activity->title }}
        <span>{{ $activity->created_at->diffForHumans() }}</span>
    @endslot

    @slot('body')
        {{ $activity->subject->body }}
    @endslot
@endcomponent
