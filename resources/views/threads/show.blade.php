@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @can ('update', $thread)
                        <div>
                            <form action="{{ $thread->path() }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button class="btn btn-primary" type="submit">Delete Thread</button>

                            </form>
                        </div>
                    @endcan

                    <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->name }}</a> Posted:
                    {{ $thread->title }}
                </div>

                <div class="card-body">
                    {{ $thread->body }}
                </div>
            </div>

            @foreach($replies as $reply)

                @include('threads.reply')

            @endforeach

            {{ $replies->links() }}

            @auth

                <form method="POST" action="{{ $thread->path() . '/replies' }}">
                    {{ csrf_field() }}
                    <textarea name="body" id="body" class="form-control" rows="5" placeholder="Add a comment"></textarea>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            @endauth

            @guest

                <p class="text-center"> <a href="{{ route('login') }}">Sign in</a> to post a reply.</p>

            @endguest

        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <a href="">{{ $thread->creator->name }}</a> Posted:
                    {{ $thread->title }}
                </div>

                <div class="card-body">
                    <p>
                        This thread was published {{ $thread->created_at->diffForHumans()  }} by
                        <a href="">{{ $thread->creator->name }}</a>, and currently
                        has {{ $thread->replies_count }} {{ str_plural('comment', $thread->replies_count) }}.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection