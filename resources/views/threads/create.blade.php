@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Forum Threads</div>

                <div class="card-body">
                    <div class="row justify-content-center">
				        <div class="col-md-8">
				            <form method="POST" action="/threads">
				                {{ csrf_field() }}

								<div class="form-group">
				                	<label for="channel_id">Choose a channel:</label>
				                	<select name="channel_id" id="channel_id" class="form-control" required>
				                		<option value="">Choose one</option>
				                		@foreach ($channels as $channel)
				                			<option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>{{ $channel->slug }}
				                			</option>
				                		@endforeach
				                	</select>
								</div>
								
								<div class="form-group">
				                	<label for="title">Title:</label>
				                	<input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}" required>
								</div>

				                <textarea name="body" id="body" class="form-control" rows="8" value="{{ old('body') }}" required>
				                	
				                </textarea>
								
								<div class="form-group">
				                	<button type="submit" class="btn btn-primary">Submit</button>
								</div>

								@if ($errors->any())
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <li>{{ $error }}</li>
								            @endforeach
								        </ul>
								    </div>
								@endif
				            </form>				        
				        </div>
				    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection